import unittest
from task_07 import file_size


class TestConverter(unittest.TestCase):

    def test_converter(self):
        self.assertEqual(file_size(999999999999), '931.3Gb')


if __name__ == '__main__':
    unittest.main()
