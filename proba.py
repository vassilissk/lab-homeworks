from collections import Counter, OrderedDict, defaultdict
a = Counter('hfkhfkjhfkljhfljhgl;;jhgljhjhzl')
print(a)
print(a.get('l'))
print(a.values())
print(a.copy())
print(a.pop('l'))
print(a.elements())
print(a.items())
print(a.most_common())
print(a.popitem())
print(a.setdefault('O',12))


b = OrderedDict({'O': 12, 'h': 8, 'j': 6, 'f': 4, 'k': 3, 'g': 2, ';': 2})
print(reversed(b))
c = defaultdict(int)
l = [i for i in range(7)]
for i in l:
    c[i] = 0
print('c=',c)