import unittest
from task_02 import filter_list_for, filter_list, filter_list_comprehensions
l = [1, 2, '3', 4, 'name', 10, 33, 'Python']


class TestFilter(unittest.TestCase):
    def setUp(self):
        pass

    def test_filter_list(self):
        self.assertEqual(filter_list(l), [1, 2, 4, 10, 33])

    def test_filter_list_for(self):
        self.assertEqual(filter_list_for(l), [1, 2, 4, 10, 33])

    def test_filter_list_comprehensions(self):
        self.assertEqual(filter_list_comprehensions(l), [1, 2, 4, 10, 33])


if __name__ == '__main__':
    unittest.main()

