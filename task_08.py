def check_ip_socket(ip_address):
    import socket
    try:
        socket.inet_aton(ip_address)
        return True
    except OSError:
        return False


def check_ip_re(ip_address):
    import re
    res = re.match('^((?:1?\d{0,2}|2[0-4]\d{1}|25[0-5])\.){3}(?:1?\d{0,2}|2[0-4]\d{1}|25[0-5])$', ip_address)
    return res


print(check_ip_socket('127.0.1'))
print(check_ip_re('127.0.1'))
