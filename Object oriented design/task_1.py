"""Создайте абстрактный класс Student. Он должен содержать следующие
аттрибуты – ID и GPA (Grade Point Average) (от 1 до 5).
Также класс должен содержать абстрактный метод get_status().​
Создайте два дочерних класса Undergraduate и Graduate.

Переопределите в классе Graduate метод get_status() так, чтобы при GPA >= 3.0
выводилась строка “Good!”, иначе – “Second-year”. ​

В классе Undergraduate переопределите метод get_status() так,
 чтобы при GPA >= 3.0 выводилась строка “Graduation with honor!”,
 при GPA от 2.0 до 3.0 выводилась строка “Good!”, иначе – “Second-year”. ​

Создайте несколько экземпляров Undergraduate и Graduate с различными GPA
и вызовите на них метод get_status().
Устанавливайте значения GPA и ID при создании инстанса класса,
передавая их в класс. Добавьте валидацию: GPA не должен быть больше 5 и
меньше 1, ID не может быть отрицательным.​"""
from abc import ABC, abstractmethod


class Student(ABC):
    @abstractmethod
    def get_status(self):
        pass


class Graduate(Student):
    def __init__(self, ID, GPA):
        if ID < 0:
            raise 'ID should be > 0'
        if GPA < 1.0 or GPA > 5.0:
            raise ValueError('Grade Point Average should be between 1 and 5')
            return
        self.ID = ID
        self.GPA = GPA

    def get_status(self):
        if self.GPA >= 3.0:
            print('Good')
        else:
            print('Second-year')


class Undergraduate(Student):
    def __init__(self, ID, GPA):
        if ID < 0:
            raise 'ID should be > 0'
        if GPA < 1.0 or GPA > 5.0:
            raise 'Grade Point Average should be between 1 and 5'
            return
        self.ID = ID
        self.GPA = GPA

    def get_status(self):
        if self.GPA >= 3.0:
            print('Graduation with honor!')
        elif 2.0 < self.GPA < 3.0:
            print('Good')
        else:
            print('Second-year')


first = Graduate(1, 2)
second = Undergraduate(2, 3)
first.get_status()
second.get_status()
