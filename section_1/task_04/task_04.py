numbers = [1, 2, '0', '300', -2.5, 'Dog', True, 0o1256, None]
result = []

for number in numbers:
    try:
        result.append(int(number))
    except Exception:
        result.append(int(bool(None)))
print(result)
max_value = max(result)
min_value = min(result)
print('max_value =', max_value)
print('min_value =', min_value)
