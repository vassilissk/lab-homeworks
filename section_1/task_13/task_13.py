import time


class timer:
    def __init__(self, text):
        self.text = text

    def __enter__(self):
        self.start = time.time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end = time.time()
        result = self.end - self.start
        print(f'Block {self.text} executed in {result} sec')


with timer("'doing some sleeps'"):
    time.sleep(1)
    time.sleep(2)
    time.sleep(3)

