q='hjghjgjg'
q.isalpha()
def check_password(password):
    password = str(password)
    if len(password) < 8:
        return 'error: too short, min length is 8'
    alpha = False
    lowercase = False
    uppercase = False
    digit = False
    for char in password:
        if char.islower():
            lowercase = True
        if char.isupper():
            uppercase = True
        if char.isdigit():
            digit = True
        if char.isalpha():
            alpha = True

    if not alpha:
        return f'error: {password} is not a string'
    if not lowercase:
        return 'error: should contain at least one lowercase letter'
    if not uppercase:
        return 'error: should contain at least one uppercase letter'
    if not digit:
        return 'error: should contain at least one digit'
    return 'Look Good'


print(check_password('Helloworld2'))

