def file_size(size_in_bytes):
    if size_in_bytes > 1024**3:
        return f'{size_in_bytes / 1024 ** 3:.1f}Gb'
    elif size_in_bytes > 1024**2:
        return f'{size_in_bytes / 1024 ** 2:.1f}Mb'
    elif size_in_bytes > 1024:
        return f'{size_in_bytes / 1024:.1f}Kb'
    else:
        return f'{size_in_bytes:.1f}B'


#print(file_size(19))
#print(file_size(12345))
#print(file_size(1101947))
#print(file_size(572090))
#print(file_size(999999999999))