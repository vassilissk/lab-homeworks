from functools import wraps
import time

def timeit(func):
    @wraps(func)
    def time_func(*args, **kwargs):
        first = time.time()
        func(*args, **kwargs)
        last = time.time()
        the_time = last - first
        print(f'The time of function execution is {round(the_time, 3)} seconds')
        #return the_time
    return time_func

@timeit
def muf():
    x = [i for i in range(10000000)]
muf()