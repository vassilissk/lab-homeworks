l = [1, [], 2, [-19, 700, 0, [90, 33, [18, 77, [0, ], -2], 11, 16], -100]]
"""function count sum of the nested list"""

def summa(the_list, res=[]):
    """function count sum of the nested list"""
    for item in the_list:
        if isinstance(item, list):
            summa(item)
        else:
            res.append(item)
    return sum(res)


print(summa(l))
