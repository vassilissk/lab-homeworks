some_list = [1, 2, '3', 4, 'name', 10, 33, 'Python']


def filter_list_for(l):
    result = []
    for item in l:
        if type(item) == int:
            result.append(item)
    return result


def filter_list_comprehensions(l):
    return [item for item in l if type(item) == int]


def filter_list(l):
    return list(filter(lambda x: type(x) == int, l))


print(filter_list_for(some_list))
print(filter_list_comprehensions(some_list))
print(filter_list(some_list))
