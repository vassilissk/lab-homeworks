import sys
import argparse


def find(folder, name=None, show_dirs=True, show_files=True):
    """
    :param folder: path to a system folder from where to start searching
    :param name: file/directory name pattern, allows using '*' and '?' symbols
    :param show_dirs: if True - include directories into search results
    :param show_files: if True - include files into search results
    """
    import os
    import fnmatch
    res = os.walk(folder)
    files = []
    dirs = []
    if show_files:
        for i in res:
            if i[2]:
                for file in i[2]:
                    if fnmatch.fnmatch(file.lower(), name):
                        files.append(os.path.join(i[0], file))
        print(f'found {len(files)} files')
        for i in files:
            print(i)
    else:
        for i in res:
            if fnmatch.fnmatch(i[0].lower(), name):
                dirs.append(i[0])
        print(f'found {len(dirs)} directories')
        for directory in dirs:
            print(directory)


def parse_cmd_args():

    path_help = "Path to a folder"
    name_help = "File name pattern. Allows using '*' and '?' symbols"
    type_help = "Where 'f' means search only files, 'd' means only directories"

    parser = argparse.ArgumentParser()
    parser.add_argument('path', help=path_help)
    parser.add_argument('-name', nargs='?', default=None, help=name_help)
    parser.add_argument('-type', nargs='?', default=None, choices=['f', 'd'], help=type_help)

    if len(sys.argv) <= 1:
        parser.print_help(sys.stderr)
        sys.exit(1)
    cmd, _ = parser.parse_known_args()

    files, dirs = True, True
    if cmd.type == 'd':
        files = False
    if cmd.type == 'f':
        dirs = False
    return cmd.path, cmd.name, dirs, files


if __name__ == '__main__':
    args = parse_cmd_args()
    find(*args)


# directory = 'D:\Python\MyPy'
# pattern = 'Py*.txt'
#
#
# def finder(directory, pattern):
#     import os
#     import fnmatch
#
#     res = os.walk(directory)
#     files = []
#     for i in res:
#         if i[2]:
#             for file in i[2]:
#                 if fnmatch.fnmatch(file, pattern):
#                     files.append(os.path.join(i[0], file))
#     return files
#
#
# def display_result(result):
#     for i in result:
#         print(i)
#

# def main():
#     args = None
#     import argparse
#     parser = argparse.ArgumentParser()
#     parser.add_argument('path')
#     parser.add_argument('-p')
#     args = parser.parse_args()
# display_result(finder(args.path, args.p))

# if __name__ == '__main__':
#     main()

#  dir = 'D:\\Python\\MyPy'
#  pattern = '*.txt'
#  res = finder(dir, pattern)
#  for file in res:
#     print(file)
