"""Homework section 2"""
list_1 = [
    "Comprehensions", 1, -1, "some", 7, 58.8, "34.46", "times ", -2, 3.6, -78.92,
    "123.1", "are", -8, "better", 3, "-46.78", 345, "than", -13.3, "'for'",
    1.2, 9, 23.8, "loops.", 4, 5
]

dict_1 = {
    "Chewie": {
        "sex": "male",
        "age": 300,
        "height": 2.2,
        "role": "pilot",
        "skills": [
            "strenght", "extra hair", "reticence"
        ]
    },
    "Anakin Skywalker": {
        "sex": "male",
        "age": 22,
        "height": 1.8,
        "role": "jedi",
        "skills": [
            "speed", "high level of midichlorians", "honesty"
        ]
    },
    "Padme Amidala": {
        "sex": "female",
        "age": 19,
        "height": 1.6,
        "role": "queen",
        "skills": [
            "honesty", "trick", "bravery"
        ]
    },
    "R2D2": {
        "sex": "unknown",
        "age": 567,
        "height": 1.1,
        "role": "robot",
        "skills": [
            "bravery", "technical skills", "size"
        ]
    },
    "Yoda": {
        "sex": "male",
        "age": 450,
        "height": 0.9,
        "role": "jedi",
        "skills": [
            "high level of midichlorians", "wisdom", "experience"
        ]
    },
    "Sabe": {
        "sex": "female",
        "age": 15,
        "height": 1.7,
        "role": "servant",
        "skills": [
            "reticence", "sensitivity", "honesty"
        ]
    },
}


def is_number(the_string: str) -> bool:
    """function take a string as input and check
    whether the string is a number or not and return bool"""
    symbols = '1234567890.-+'
    for item in the_string:
        if item not in symbols:
            return False
    return True


def squares(the_list: list) -> list:
    """REQUIREMENTS:
    Derive from list_1 a list of squares of all positive int values that are divisible by
    2 without a remainder using List Comprehensions output: list"""
    return [item ** 2 for item in the_list if isinstance(item, int)
            and item > 0 and item % 2 == 0]


def cubes(the_list: list) -> list:
    """REQUIREMENTS:
    Output from list_1 a list of cubes of all negative float values that are NOT
    divisible by 2 without a remainder, the number decimal places: 2 using List Comprehensions
    output: list"""
    return [round(item ** 3, 2) for item in the_list if isinstance(item, float)
            and item < 0 and item % 2 != 0]


def sentence(the_list: list) -> str:
    """REQUIREMENTS:
    Collect a sentence from words from list_1 using is_number using List Comprehensions
    output: str"""
    return ' '.join([item for item in the_list if not is_number(str(item))])


def negative(the_list: list) -> float:
    """REQUIREMENTS:
    Calculate the sum of all negative values from list_1 using is_number and List Comprehensions
    output: float"""
    return sum([float(item) for item in the_list if is_number(str(item)) and float(item) < 0])


sort_by_height_desc = 'qwerty'

"""REQUIREMENTS:Write a generator that will take the sort_by_height_desc variable as input and"""


def gen(var):
    for item in var:
        yield item


gen_2 = (item for item in sort_by_height_desc)
gen_1 = gen(sort_by_height_desc)
for i in gen_1:
    print(i)

print(is_number('123p'))
print(squares(list_1))
print(cubes(list_1))
print(sentence(list_1))
print(negative(list_1))
